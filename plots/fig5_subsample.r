# load packages & functions ----
rm(list=ls())
library(data.table)
library(dplyr)
library(Smisc)
library(forecast)
library(plyr)
library(viridis)
library(ggplot2)
library(RColorBrewer)
library(readr)
library(grid)
library(VennDiagram)
library(pheatmap)
# load data ----
taps <- read_delim("processed/taps_pub.average.methratio.txt",delim="\t",col_names=TRUE)
e14_snp <- read_delim("processed/e14.cgsnp.bed",delim="\t", col_names=FALSE)
colnames(e14_snp) <- c("chr","start","end","snp")

all_taps <- taps
rm(taps)
for(i in list.files(path = "processed",pattern="Enrichment_TAPS_bwamem.merge.mm9.sort.*proper.meth.txt")){
      tmp <- read_delim(paste0("processed/",i), delim="\t",col_names=TRUE)
      all_taps <- merge(all_taps,tmp,by.x=c("chr","start"),by.y=c("chr","pos"))
      rm(tmp)
}

colnames(all_taps) <- c("chr","start","taps_mC","taps_aC","taps_rmC",
                        "user_total_0.1","user_cleaved_0.1","user_uncleaved_0.1","user_methratio_0.1",
                        "user_total_0.2","user_cleaved_0.2","user_uncleaved_0.2","user_methratio_0.2",
                        "user_total_0.3","user_cleaved_0.3","user_uncleaved_0.3","user_methratio_0.3",
                        "user_total_0.4","user_cleaved_0.4","user_uncleaved_0.4","user_methratio_0.4",
                        "user_total_0.5","user_cleaved_0.5","user_uncleaved_0.5","user_methratio_0.5",
                        "user_total_0.6","user_cleaved_0.6","user_uncleaved_0.6","user_methratio_0.6",
                        "user_total_0.7","user_cleaved_0.7","user_uncleaved_0.7","user_methratio_0.7",
                        "user_total_0.8","user_cleaved_0.8","user_uncleaved_0.8","user_methratio_0.8",
                        "user_total_0.9","user_cleaved_0.9","user_uncleaved_0.9","user_methratio_0.9",
                        "user_total_1.0","user_cleaved_1.0","user_uncleaved_1.0","user_methratio_1.0"
                        )                      

all_taps <- all_taps[order(all_taps$chr,all_taps$start),]
all_taps <- merge(all_taps,e14_snp,by=c("chr","start"),all.x=TRUE)
all_taps <- all_taps[!complete.cases(all_taps$end),-((ncol(all_taps)-1):ncol(all_taps))]

covered_C1 <- 5 # depth_filter for wgTAPS and rrTAPS
covered_C2 <- 1 # depth_filter eeTAPS
outfix <- "plots/depth1_depth5"


seltaps <- all_taps[all_taps$taps_aC>=covered_C1,]
methratio <- 0.1
seltaps$user_0.1 <- apply(seltaps %>% dplyr::select(user_total_0.1, user_methratio_0.1), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.2 <- apply(seltaps %>% dplyr::select(user_total_0.2, user_methratio_0.2), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.3 <- apply(seltaps %>% dplyr::select(user_total_0.3, user_methratio_0.3), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.4 <- apply(seltaps %>% dplyr::select(user_total_0.4, user_methratio_0.4), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.5 <- apply(seltaps %>% dplyr::select(user_total_0.5, user_methratio_0.5), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.6 <- apply(seltaps %>% dplyr::select(user_total_0.6, user_methratio_0.6), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.7 <- apply(seltaps %>% dplyr::select(user_total_0.7, user_methratio_0.7), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.8 <- apply(seltaps %>% dplyr::select(user_total_0.8, user_methratio_0.8), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_0.9 <- apply(seltaps %>% dplyr::select(user_total_0.9, user_methratio_0.9), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$user_1.0 <- apply(seltaps %>% dplyr::select(user_total_1.0, user_methratio_1.0), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > methratio, 1, 0)})
seltaps$taps <- ifelse(seltaps$taps_rmC=="NA","NA",ifelse(seltaps$taps_rmC>methratio,1,0))

# fig 5a ----
mCsta <- merge(table(seltaps%>% dplyr::select(taps,user_0.1),useNA="ifany",dnn=c("taps","user")) %>% as.data.frame(),
               table(seltaps%>% dplyr::select(taps,user_0.2),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_0.3),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_0.4),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_0.5),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_0.6),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_0.7),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_0.8),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_0.9),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))%>%
         merge(table(seltaps%>% dplyr::select(taps,user_1.0),useNA="ifany", dnn=c("taps","user")) %>% as.data.frame(), by=c("taps","user"))
colnames(mCsta) <- c("taps","eetaps","user_0.1","user_0.2","user_0.3","user_0.4","user_0.5","user_0.6","user_0.7","user_0.8","user_0.9","user_1.0")
mCsta[mCsta$taps=="1" & mCsta$eetaps=="1" & complete.cases(mCsta),-c(1,2)]/17345472 
#  user_0.1 user_0.2 user_0.3 user_0.4 user_0.5 user_0.6 user_0.7 user_0.8  user_0.9 user_1.0
#  10144261 13003348 14344193 15098542 15571272 15884774 16104089 16264256 16384301 16476625


mCsta.m <- melt(mCsta,id.vars=c("taps","eetaps"))
p <- ggplot(mCsta.m[complete.cases(mCsta.m) & mCsta.m$taps==1&mCsta.m$eetaps==1,], aes(x=variable,y=value,fill=taps), alpha=0.8) + 
  geom_bar(stat="identity",width=0.5) +
  scale_fill_manual(values=c("#446db4"),labels = c("detected by TAPS"),name="") +
  theme(legend.position="none",
      panel.grid.major = element_blank(), 
      panel.grid.minor = element_blank(),
      panel.background = element_blank(), 
      axis.line = element_line(colour = "black"))
ggsave(paste0(outfix,"subsample_mC.sta.pdf"), p, width=6,height =3.5 )

# fig 5b ----
##### chr #####
winsize <- 100000
seldat <- seltaps
seldat$index <- round(seldat$start/winsize)
taps <- aggregate(taps_rmC ~ chr+index,seldat,mean)
user_0.1_mc <- aggregate(user_methratio_0.1 ~ chr+index,seldat[seldat$user_total_0.1>=covered_C2,],mean) 
user_0.2_mc <- aggregate(user_methratio_0.2 ~ chr+index,seldat[seldat$user_total_0.2>=covered_C2,],mean) 
user_0.3_mc <- aggregate(user_methratio_0.3 ~ chr+index,seldat[seldat$user_total_0.3>=covered_C2,],mean) 
user_0.4_mc <- aggregate(user_methratio_0.4 ~ chr+index,seldat[seldat$user_total_0.4>=covered_C2,],mean) 
user_0.5_mc <- aggregate(user_methratio_0.5 ~ chr+index,seldat[seldat$user_total_0.5>=covered_C2,],mean) 
user_0.6_mc <- aggregate(user_methratio_0.6 ~ chr+index,seldat[seldat$user_total_0.6>=covered_C2,],mean) 
user_0.7_mc <- aggregate(user_methratio_0.7 ~ chr+index,seldat[seldat$user_total_0.7>=covered_C2,],mean) 
user_0.8_mc <- aggregate(user_methratio_0.8 ~ chr+index,seldat[seldat$user_total_0.8>=covered_C2,],mean) 
user_0.9_mc <- aggregate(user_methratio_0.9 ~ chr+index,seldat[seldat$user_total_0.9>=covered_C2,],mean) 
user_1.0_mc <- aggregate(user_methratio_1.0 ~ chr+index,seldat[seldat$user_total_1.0>=covered_C2,],mean) 


selmC <- merge(taps,user_0.1_mc, by=c("chr","index")) %>% 
      merge(user_0.2_mc,by=c("chr","index"))%>%
      merge(user_0.3_mc,by=c("chr","index"))%>%
      merge(user_0.4_mc,by=c("chr","index"))%>%
      merge(user_0.5_mc,by=c("chr","index"))%>%
      merge(user_0.6_mc,by=c("chr","index"))%>%
      merge(user_0.7_mc,by=c("chr","index"))%>%
      merge(user_0.8_mc,by=c("chr","index"))%>%
      merge(user_0.9_mc,by=c("chr","index"))%>%
      merge(user_1.0_mc,by=c("chr","index"))
corchr <- cor(selmC[,-c(1:2)])
pheatmap(selmC[order(selmC$chr,selmC$index),-c(1:2)], cluster_cols=FALSE, cluster_rows= FALSE, show_rownames = FALSE, filename=paste0(outfix,"chr_100k.pdf"))
##### cgi #####
regionfile <- c("processed/cpgIsland.4k.intersectcpg.txt")
cgi <- fread(regionfile, header=F, stringsAsFactors = F) %>% as.data.frame()
colnames(cgi) <- c("gchr","gstart","gend","id","index","chr","start","end")
cgi <- merge(cgi, seltaps, by=c("chr","start"))
selcgi <- cgi[cgi$index>50 & cgi$index<71, ]

taps <- aggregate(taps_rmC ~ id, selcgi, mean)
user_0.1_mc <- aggregate(user_methratio_0.1 ~ id, selcgi[selcgi$user_total_0.1>=covered_C2,], mean) 
user_0.2_mc <- aggregate(user_methratio_0.2 ~ id, selcgi[selcgi$user_total_0.2>=covered_C2,], mean) 
user_0.3_mc <- aggregate(user_methratio_0.3 ~ id, selcgi[selcgi$user_total_0.3>=covered_C2,], mean) 
user_0.4_mc <- aggregate(user_methratio_0.4 ~ id, selcgi[selcgi$user_total_0.4>=covered_C2,], mean) 
user_0.5_mc <- aggregate(user_methratio_0.5 ~ id, selcgi[selcgi$user_total_0.5>=covered_C2,], mean) 
user_0.6_mc <- aggregate(user_methratio_0.6 ~ id, selcgi[selcgi$user_total_0.6>=covered_C2,], mean) 
user_0.7_mc <- aggregate(user_methratio_0.7 ~ id, selcgi[selcgi$user_total_0.7>=covered_C2,], mean) 
user_0.8_mc <- aggregate(user_methratio_0.8 ~ id, selcgi[selcgi$user_total_0.8>=covered_C2,], mean) 
user_0.9_mc <- aggregate(user_methratio_0.9 ~ id, selcgi[selcgi$user_total_0.9>=covered_C2,], mean) 
user_1.0_mc <- aggregate(user_methratio_1.0 ~ id, selcgi[selcgi$user_total_1.0>=covered_C2,], mean) 

selmC <- merge(taps,user_0.1_mc, by=c("id")) %>% 
      merge(user_0.2_mc,by=c("id"))%>%
      merge(user_0.3_mc,by=c("id"))%>%
      merge(user_0.4_mc,by=c("id"))%>%
      merge(user_0.5_mc,by=c("id"))%>%
      merge(user_0.6_mc,by=c("id"))%>%
      merge(user_0.7_mc,by=c("id"))%>%
      merge(user_0.8_mc,by=c("id"))%>%
      merge(user_0.9_mc,by=c("id"))%>%
      merge(user_1.0_mc,by=c("id"))
corcgi <- cor(selmC[,-c(1)])


cors <- cbind(corchr[-1,11],corcgi[-1,11]) %>% as.data.frame()
cors$sample <- rownames(cors)
colnames(cors) <- c("chr","cgi","sample")
cors.m <- melt(cors,by="sample")
p <- ggplot(data=cors.m, aes(x=sample, y=value, group=variable)) +
  geom_line(aes(color=variable))+
  geom_point(aes(color=variable))+
  ylim(0,1)+
  scale_color_manual(values=c("#23b177","#446db4"),labels = c("on 100k", "cgi"),name="") +
  theme(panel.grid.major = element_blank(), 
      panel.grid.minor = element_blank(),
      panel.background = element_blank(), 
      axis.line = element_line(colour = "black"))

ggsave(paste0(outfix,"cors.pdf"), p, width=6,height =3 )

#                          chr       cgi             sample
# user_methratio_0.1 0.9324717 0.5538631 user_methratio_0.1
# user_methratio_0.2 0.9662629 0.6551673 user_methratio_0.2
# user_methratio_0.3 0.9794115 0.7491875 user_methratio_0.3
# user_methratio_0.4 0.9875376 0.8314015 user_methratio_0.4
# user_methratio_0.5 0.9917264 0.8999107 user_methratio_0.5
# user_methratio_0.6 0.9949496 0.9283040 user_methratio_0.6
# user_methratio_0.7 0.9969922 0.9552924 user_methratio_0.7
# user_methratio_0.8 0.9984611 0.9703074 user_methratio_0.8
# user_methratio_0.9 0.9993353 0.9872630 user_methratio_0.9
# user_methratio_1.0 1.0000000 1.0000000 user_methratio_1.0

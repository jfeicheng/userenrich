# load packages & functions ----
.libPaths(c(.libPaths(),"/gpfs2/well/ludwig/users/cfo155/miniconda2/lib/R/library/"))
library(data.table)
library(dplyr)
library(Smisc)
library(forecast)
library(plyr)
library(viridis)
library(ggplot2)
library(RColorBrewer)
library(readr)
library(grid)
library(VennDiagram)
library(pryr)
library(Smisc)
library(gridExtra)

get_density <- function(x, y, ...) {
  dens <- MASS::kde2d(x, y, ...)
  ix <- findInterval(x, dens$x)
  iy <- findInterval(y, dens$y)
  ii <- cbind(ix, iy)
  return(dens$z[ii])
}
# load data ----
taps <- read_delim("processed/taps_pub.average.methratio.txt",delim="\t",col_names=TRUE)
user_taps <- read_delim("processed/Enrichment_TAPS_new_bwamem.mm9.sort.proper.meth.txt",delim="\t",col_names=TRUE)
all_taps <- merge(taps,user_taps,by.x=c("chr","start"),by.y=c("chr","pos"))
colnames(all_taps) <- c("chr","start","taps_mC","taps_aC","taps_rmC","user_total","user_cleaved","user_uncleaved","user_rmC")
all_taps <- all_taps[order(all_taps$chr,all_taps$start),]
e14_snp <- read_delim("processed/e14.cgsnp.bed",delim="\t", col_names=FALSE)
colnames(e14_snp) <- c("chr","start","end","snp")
all_taps <- merge(all_taps,e14_snp,by=c("chr","start"),all.x=TRUE)
all_taps <- all_taps[!complete.cases(all_taps$end),-c((ncol(all_taps)-1):ncol(all_taps))]

covered_C1 <- 5 # depth_filter for wgTAPS and rrTAPS
covered_C2 <- 1 # depth_filter eeTAPS
outfix <- "plots/depth1_depth5"
seltaps <- all_taps[all_taps$user_total>=covered_C2 & all_taps$taps_aC>=covered_C1,]


# fig 3a ----
winsize <- 100000
seldat <- seltaps
seldat$index <- round(seldat$start/winsize)
taps <- aggregate(taps_rmC ~ chr+index, seldat,mean)
userrc <- aggregate(user_rmC ~ chr+index, seldat, mean)
selmC <- merge(taps, userrc,by=c("chr","index")) 
chr <- "chr1"
selmeth <- selmC[selmC$chr==chr,]
selmeth <- selmeth[order(selmeth$index),]
selmeth$taps_meth.s <- movAvg2(selmeth$taps_rmC, bw = 5, type = c("gaussian"))
selmeth$user_meth.s <- movAvg2(selmeth$user_rmC, bw = 5, type = c("gaussian"))
selmeth.m <- selmeth %>% dplyr::select(chr,index,taps_meth.s,user_meth.s) %>% melt(id.vars=c("chr","index"))
p <- ggplot(selmeth.m, aes(x=index,y=value,color=variable)) +
  geom_line(size=0.7)+
  theme(legend.position = "bottom")+
  ylim(0,1) +
  ylab("methylation ratio") +
  xlab(paste0(chr,"\n(window size:",winsize,")")) +
  scale_color_manual(values=c("#23b177","#446db4"),name="") +
  theme(
    panel.grid.major = element_blank(), 
    panel.grid.minor = element_blank(),
    panel.background = element_blank(), 
    axis.line = element_line(colour = "black"))
ggsave(paste0(outfix, "taps.vs.usertaps.",chr,"dis.pdf"),p, width=6,height = 2 )
# fig 3b ----
cor(selmC %>% dplyr::select(taps_rmC, user_rmC))
#          taps_rmC  user_rmC
# taps_rmC 1.0000000 0.7829113
# user_rmC 0.7829113 1.0000000
pdf(paste0(outfix, "taps.vs.usertaps.","chr.cor.smoothscatter.pdf"), width = 3.2, height = 3.5)
smoothScatter(x=selmC$taps_rmC,
              y=selmC$user_rmC,
              xlab=paste0("wgTAPS\n(window size:",winsize,")\n cor:",cor(selmC %>% dplyr::select(taps_rmC, user_rmC))),
              ylab= "eeTAPS",
              xlim=c(0,1),
              ylim=c(0,1))
dev.off()


# fig 3d ----
regionfile <- c("processed/cpgIsland.4k.intersectcpg.txt")
cgi <- fread(regionfile, header=F, stringsAsFactors = F) %>% as.data.frame()
colnames(cgi) <- c("gchr","gstart","gend","id","index","chr","start","end")
cgi <- merge(cgi, seltaps, by=c("chr","start"))

selcgi <- cgi[cgi$index>50 & cgi$index<71, ]
taps <- aggregate(taps_rmC ~ id, selcgi, mean)
userrc <- aggregate(user_rmC ~ id, selcgi, mean)


selmC <- merge(taps, userrc, by=c("id"))
selmC$density <- get_density(selmC$taps_rmC, selmC$user_rmC, n = 200)
cormeth <- round(cor(selmC$taps_rmC,selmC$user_rmC),2)
cor(selmC %>% dplyr::select(taps_rmC, user_rmC))
#          taps_rmC  user_rmC
# taps_rmC 1.0000000 0.7508548
# user_rmC 0.7508548 1.0000000
pdf(paste0(outfix,"cpgIsland.4k.intersectcpg.cor.pdf.smoothscatter.pdf"), width = 3.2, height = 3.5)
smoothScatter(x=selmC$taps_rmC,
              y=selmC$user_rmC,
              xlab="wgTAPS", 
              ylab="eeTAPS"
)
dev.off()
# fig 3c ----
taps <- aggregate(taps_rmC ~ id +index,cgi,mean)
userrc <- aggregate(user_rmC ~ id + index, cgi, mean)
selmC <- merge(taps, userrc, by=c("id","index"))

taps.mean <- aggregate(taps_rmC ~ index,selmC,mean)
usermc.mean <- aggregate(user_rmC ~ index, selmC, mean)
cgimeth <- merge(taps.mean, usermc.mean,by=c("index")) %>% melt(id.vars=c("index")) 
p1 <- ggplot(cgimeth, aes(x=index,y=value,color=variable)) +
  geom_line()+
  theme(legend.position = "bottom") +
  ylim(0,1) +
  ylab("methylation ratio") +
  xlab("around CGI") +
  scale_x_continuous(breaks=c(0,50,70,120),
                     labels=c("CpG-4k", "                CpG island","", "CpG+4k"))+
  scale_color_manual(values=c("#446db4","#23b177"),name="")+
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        panel.background = element_blank(), 
        axis.line = element_line(colour = "black"))
ggsave(paste0(outfix,"cpgIsland.4k.intersectcpg.pdf"), p1, width=3,height =3 )

# fig 3e ----
regionfile <- c("processed/mESC_cStates_HMM.intersectcpg.txt")
regions <- fread(regionfile, header=F, stringsAsFactors = F) %>% as.data.frame()
colnames(regions) <- c("gchr","gstart","gend","id","chr","start","end")
regionsmeth <- merge(regions, seltaps, by=c("chr","start"))
regionsmeth$id <- gsub("[0-9]*_","",regionsmeth$id)

taps <- aggregate(taps_rmC ~ id+gchr+gstart+gend, regionsmeth, mean)
usermc <- aggregate(user_rmC ~ id+gchr+gstart+gend, regionsmeth, mean)

selmC <- merge(taps, usermc, by=c("id","gchr","gstart","gend")) 
selmC$len <- selmC$gend - selmC$gstart
selmC$id <- gsub("[0-9]*_","",selmC$id)
# sort by mean methylation #
meantaps <- aggregate(taps_rmC ~ id, selmC, mean)
labels <- meantaps[order(meantaps$taps_rmC),1]
selmC$id <- factor(selmC$id, levels = labels)

selmC.m <- melt(selmC[,c(1,5,6)], id.vars = c("id"))
selmC.m$id <- factor(selmC.m$id, level=c("ActivePromoter","PoisedPromoter","PoisedEnhancer","Insulator","StrongEnhancer","TxnTransition","Repressed","WeakTxn","Heterochrom","TxnElongation"))

p <- ggplot(selmC.m, aes(x=id,y=value,fill=variable, alpha=0.8))+
  geom_boxplot(outlier.shape = NA) +
  scale_fill_manual(values=c("#23b177","#446db4"),name="")+
  theme_minimal()+
  ylab("methylation ratio") +
  xlab("") +
  theme(legend.position="bottom",
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        panel.background = element_blank(), 
        axis.text.x = element_text(angle=45,vjust=0.5),
        axis.line = element_line(colour = "black"))
ggsave(paste0(outfix,"mESC_cStates_HMM.intersectmeth.pdf"), p, width=6,height =3.5 )

# fig 3f ----
cormeth <- round(cor(selmC$taps_rmC,selmC$user_rmC),2)
# 0.79
pdf(paste0(outfix,"mESC_cStates_HMM.intersectmeth.cor.smoothscatter.pdf"), width = 3.5, height = 3.5)
smoothScatter(x=selmC$taps_rmC,
              y=selmC$user_rmC,
              xlab="TAPS average methylation ratio",
              ylab= "userTAPS mC/(mC+uC)",
              xlim=c(0,1),
              ylim=c(0,1))
dev.off()


# fig s3b ----
# gene expression
gene_expr <- read.table("processed/rna_seq.featurecounts.txt",header=T)
norn <- c(62191073, 33746060)
gene_expr$SRR5093513_rpkm <- gene_expr$SRR5093513/(gene_expr$Length/1000)/(norn[1]/1000000)
gene_expr$SRR5093514_rpkm <- gene_expr$SRR5093514/(gene_expr$Length/1000)/(norn[2]/1000000)
gene_expr$expr_mean <- (gene_expr$SRR5093513_rpkm + gene_expr$SRR5093513_rpkm)/2
gene_expr$expr_n <- cut(gene_expr$expr_mean , 
                        breaks= c(0,
                                  quantile(gene_expr$expr_mean[gene_expr$expr_mean>0],probs=seq(0,1,1/3))),
                        include.lowest = T,
                        labels =c("no_expr","low_expr","mid_expr","high_expr"))
# tss
regionfile <- c("processed/gencode.vM1.annotation.tss5k.n100.intersectcpg.txt")
tss <- fread(regionfile, header=F, stringsAsFactors = F) %>% as.data.frame()
colnames(tss) <- c("gchr","gstart","gend","id","index","chr","start","end")
tss <- merge(tss, seltaps, by=c("chr","start"))

taps <- aggregate(taps_rmC ~ id+index,tss,mean)
userrc <- aggregate(user_rmC ~ id+index, tss, mean)
selmC <- merge(taps, userrc, by=c("id","index")) 

selmC$Geneid <- gsub("\\(+\\)|\\(-\\)","",selmC$id)
selmC <- merge(selmC,gene_expr[,c(1,8)],by=c("Geneid"))
taps.mean <- aggregate(taps_rmC ~ index+expr_n,selmC,mean)
usermc.mean <- aggregate(user_rmC ~ index+expr_n, selmC, mean)
tssmeth <- merge(taps.mean, usermc.mean,by=c("index","expr_n")) %>% melt(id.vars=c("index","expr_n")) 
p1 <- ggplot(tssmeth, aes(x=index,y=value,color=expr_n)) +
  geom_line()+
  ylim(0,1) +
  ylab("methylation ratio") +
  xlab("around tss") +
  scale_x_continuous(breaks=c(0,50,100),
                     labels=c("TSS-5k", "TSS", "TSS+5k"))+
  facet_wrap(~variable) +
  scale_color_manual(values=brewer.pal(4,"Dark2")[c(3,1,2,4)],name="") +
  theme(legend.position="bottom",
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        panel.background = element_blank(), 
        axis.line = element_line(colour = "black"))
ggsave(paste0(outfix,"gencode.vM1.annotation.tss5k.n100.intersectcpg.expr.pdf"), p1, width=7,height =3.8 )
# tes
regionfile <- c("/users/ludwig/cfo155/cfo155/userEnrich/pub/gencode.vM1.annotation.tes5k.n100.intersectcpg.txt")
tes <- fread(regionfile, header=F, stringsAsFactors = F) %>% as.data.frame()
colnames(tes) <- c("gchr","gstart","gend","id","index","chr","start","end")
tes <- merge(tes, seltaps, by=c("chr","start"))

taps <- aggregate(taps_rmC ~ id+index,tes,mean)
userrc <- aggregate(user_rmC ~ id+index, tes, mean)
selmC <- merge(taps, userrc, by=c("id","index")) 

selmC$Geneid <- gsub("\\(+\\)|\\(-\\)","",selmC$id)
selmC <- merge(selmC,gene_expr[,c(1,8)],by=c("Geneid"))
taps.mean <- aggregate(taps_rmC ~ index+expr_n,selmC,mean)
usermc.mean <- aggregate(user_rmC ~ index+expr_n, selmC, mean)
tesmeth <- merge(taps.mean, usermc.mean,by=c("index","expr_n")) %>% melt(id.vars=c("index","expr_n")) 

p1 <- ggplot(tesmeth, aes(x=index,y=value,color=expr_n)) +
  geom_line()+
  ylim(0,1) +
  ylab("methylation ratio") +
  xlab("around tes") +
  scale_x_continuous(breaks=c(0,50,100),
                     labels=c("TES-5k", "TES", "TES+5k"))+
  facet_wrap(~variable) +
  scale_color_manual(values=brewer.pal(4,"Dark2")[c(3,1,2,4)],name="") +
  theme(legend.position="bottom",
        panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank(),
        panel.background = element_blank(), 
        axis.line = element_line(colour = "black"))
ggsave(paste0(outfix,"gencode.vM1.annotation.tes5k.n100.intersectcpg.expr.pdf"), p1, width=7,height =3.8 )


# fig s3a,c,d ----
taps <- read_delim("processed/taps_pub.average.methratio.txt",delim="\t",col_names=TRUE)
user_taps <- read_delim("processed/Enrichment_TAPS_new_bwamem.mm9.sort.proper.meth.txt",delim="\t",col_names=TRUE)
all_taps <- merge(taps,user_taps,by.x=c("chr","start"),by.y=c("chr","pos"))
colnames(all_taps) <- c("chr","start","taps_mC","taps_aC","taps_rmC","user_total","user_cleaved","user_uncleaved","user_rmC")
all_taps <- all_taps[order(all_taps$chr,all_taps$start),]
e14_snp <- read_delim("processed/e14.cgsnp.bed",delim="\t", col_names=FALSE)
colnames(e14_snp) <- c("chr","start","end","snp")
all_taps <- merge(all_taps,e14_snp,by=c("chr","start"),all.x=TRUE)
all_taps <- all_taps[!complete.cases(all_taps$end),-c((ncol(all_taps)-1):ncol(all_taps))]

covered_C1 <- 5 # depth_filter for wgTAPS and rrTAPS
covered_C2 <- 1 # depth_filter eeTAPS
outfix <- "plots/depth1_depth5"
all_taps$taps_cC <- ifelse(all_taps$taps_aC >= covered_C1,1,0); all_taps$taps_cC[is.na(all_taps$taps_cC)] <- 0
all_taps$user_cC <- ifelse(all_taps$user_total >= covered_C2,1,0); all_taps$user_cC[is.na(all_taps$user_cC)] <- 0
ratio <- 0.1
all_taps$taps <- ifelse(all_taps$taps_rmC > ratio & all_taps$taps_aC >= covered_C1, 1, 0); all_taps$taps[is.na(all_taps$taps)] <- 0
all_taps$user <- ifelse(all_taps$user_rmC > ratio & all_taps$user_total >= covered_C2, 1, 0); all_taps$user[is.na(all_taps$user)] <- 0

regionfile <- c("processed/mESC_cStates_HMM.intersectcpg.txt")
regions <- fread(regionfile, header=F, stringsAsFactors = F) %>% as.data.frame()
colnames(regions) <- c("gchr","gstart","gend","id","chr","start","end")
regionsmeth <- merge(regions, all_taps, by=c("chr","start"))
regionsmeth$id <- gsub("[0-9]*_","",regionsmeth$id)

taps_cC <- aggregate(taps_cC ~ id+gchr+gstart+gend, regionsmeth, sum)
taps_mC <- aggregate(taps ~ id+gchr+gstart+gend, regionsmeth, sum)
taps_meth <- aggregate(taps_rmC ~ id+gchr+gstart+gend, regionsmeth, mean)
user_meth <- aggregate(user_rmC ~ id+gchr+gstart+gend, regionsmeth, mean)
taps_sta <- merge(taps_cC, taps_mC,by=c("gchr","gstart","gend","id")) %>%
  merge(taps_meth,by=c("gchr","gstart","gend","id")) %>%
  merge(user_meth,by=c("gchr","gstart","gend","id"))
taps_sta$methdensity <- taps_sta$taps/(taps_sta$gend-taps_sta$gstart)
taps_sta$methpct <- taps_sta$taps/taps_sta$taps_cC
## groups regions by its mC density ##
taps_sta$methdensity_level <- cut(taps_sta$methdensity,breaks=quantile(taps_sta$methdensity,probs=seq(0,1,1/5)),include.lowest = TRUE, labels=paste0("density",seq(1,5,1)))
taps_sta$diff <- taps_sta$user_rmC - taps_sta$taps_rmC

p1 <- ggplot(taps_sta %>% dplyr::select(gchr,gstart,gend,id,diff,methdensity_level) %>% melt(id.vars=c("gchr","gstart","gend","id","methdensity_level")),aes(x=methdensity_level,y=value)) +
  geom_hline(yintercept=0, linetype="dashed", color = "red", size=2) +
  geom_boxplot(outlier.shape = NA) + 
  theme_minimal() + 
  theme(axis.text.x = element_text(angle = 90)) + ylab("eetaps - wgtaps") 
feature_methden <-as.data.frame(table(taps_sta %>% dplyr::select("id","methdensity_level")) / apply(table(taps_sta %>% dplyr::select("id","methdensity_level")) ,1,sum)) 
feature_methden$id <- factor(feature_methden$id, level=c("ActivePromoter","PoisedPromoter","PoisedEnhancer","Insulator","StrongEnhancer","TxnTransition","Repressed","WeakTxn","Heterochrom","TxnElongation"))

p2 <- ggplot(feature_methden, aes(x=id,y=Freq,fill=methdensity_level))+
  geom_bar(stat="identity") + scale_fill_manual(values=brewer.pal(9,"RdBu")[c(2,4,5,6,8)]) +
  blank_theme+theme(axis.text.x = element_text(angle = 90), legend.position = "bottom")



winsize <- 100000
seltaps <- all_taps[all_taps$user_total>=covered_C2 & all_taps$taps_aC>=covered_C1,]
seldat <- seltaps
seldat$index <- round(seldat$start/winsize)
taps_chr <- aggregate(taps_rmC ~ chr+index, seldat,mean)
user_chr <- aggregate(user_rmC ~ chr+index, seldat, mean)
selmC <- merge(taps_chr, user_chr,by=c("chr","index")) 
selmC <- selmC[order(selmC$chr, selmC$index),]
selmeth <- selmC[selmC$chr=="chr1",]
selmeth$diff <- selmeth$user_rmC- selmeth$taps_rmC
selmeth$diff.s <- movAvg2(selmeth$diff, bw = 10, type = c("gaussian"))

p3 <- ggplot(selmeth %>% dplyr::select("chr","index","diff.s") %>% melt(id.vars=c("chr","index")), aes(x=index,y=value,color=variable)) +
  geom_line()+
  ylab("eetaps - wgtaps") +
  blank_theme + theme(legend.position = "bottom")

taps_meth <- taps[taps$uC >= covered_C1 & taps$taps_meth>0.1,]
taps_meth$idx <- round(taps_meth$start/100000)
taps_meth_den <- aggregate(start ~ chr+idx,taps_meth,length)
colnames(taps_meth_den) <- c("chr","idx","den")
taps_meth_den <- taps_meth_den[order(taps_meth_den$chr,taps_meth_den$idx),]
taps_meth_den <- taps_meth_den[taps_meth_den$chr=="chr1",]
taps_meth_den$den.s <- movAvg2(taps_meth_den$den, bw = 10, type = c("gaussian"))

p4 <- ggplot(taps_meth_den) + 
  geom_line(aes(x=idx,y=den.s)) + 
  ylab("mC density") + blank_theme + theme(legend.position = "bottom")
p <- grid.arrange(arrangeGrob(p1, p2, ncol = 2),                           
                  arrangeGrob(p3, p4, ncol = 1),
                  nrow = 2)       
ggsave(paste0(outfix,"eetaps_density_bias.pdf"),p,width=13,height=10)




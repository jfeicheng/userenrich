# load packages & functions ----
.libPaths(c(.libPaths(),"/gpfs2/well/ludwig/users/cfo155/miniconda2/lib/R/library/"))
library(readr)
library(dplyr)
library(reshape2)
library(ggplot2)
library(viridis)
library(RColorBrewer)
library(grid)
library(VennDiagram)
library(pheatmap)
library(data.table)
library(UpSetR)
library(tidyr)
library(memisc)

blank_theme <- theme_minimal()+
  theme(
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    panel.border = element_blank(),
    panel.grid=element_blank(),
    axis.ticks = element_blank(),
    plot.title=element_text(size=14, face="bold")
  )
# load data ----
user_taps <- read_delim("processed/Enrichment_TAPS_new_bwamem.mm9.sort.proper.meth.txt",delim="\t",col_names=TRUE) %>% as.data.frame()
taps <- read_delim("processed/taps_pub.average.methratio.txt",delim="\t",col_names=TRUE) %>% as.data.frame()
rrtaps <- read_delim("processed/rrTAPS_rep2_mCtoT_CpG.average.methratio.txt",delim="\t",col_names=TRUE) %>% as.data.frame()
all_taps <- cbind(taps,rrtaps) %>% cbind(user_taps)
all_taps <- all_taps[,-c(6,7,11,12)]
colnames(all_taps) <- c("chr","start","taps_mC","taps_aC","taps_rmC",
                        "rrtaps_mC","rrtaps_aC","rrtaps_rmC","user_total","user_cleaved","user_uncleaved","user_rmC")
# remove snp
e14_snp <- read_delim("processed/e14.cgsnp.bed",delim="\t", col_names=FALSE)
colnames(e14_snp) <- c("chr","start","end","snp")
all_taps <- merge(all_taps,e14_snp,by=c("chr","start"),all.x=TRUE)
all_taps <- all_taps[!complete.cases(all_taps$end),-c(ncol(all_taps)-1,ncol(all_taps))]

# # fig 2b ----
## covered C sites
covered_C1 <- 5 # depth_filter for wgTAPS and rrTAPS
covered_C2 <- 1 # depth_filter eeTAPS
outfix <- "plots/depth1_depth5"
all_taps$taps_cC <- ifelse(all_taps$taps_aC >= covered_C1,1,0); all_taps$taps_cC[is.na(all_taps$taps_cC)] <- 0
all_taps$rrtaps_cC <- ifelse(all_taps$rrtaps_aC >= covered_C1,1,0); all_taps$rrtaps_cC[is.na(all_taps$rrtaps_cC)] <- 0
all_taps$user_cC <- ifelse(all_taps$user_total >= covered_C2,1,0); all_taps$user_cC[is.na(all_taps$user_cC)] <- 0
covered_C_sta <- as.data.frame(paste(all_taps$taps_cC, all_taps$rrtaps_cC,all_taps$user_cC,sep="_") %>% table() /1000000)
covered_C_sta
num_all_taps <- sum(all_taps$taps_aC>=covered_C1, na.rm=TRUE) ; num_all_taps # 19170851
num_all_user <- sum(all_taps$user_total>=covered_C2, na.rm=TRUE) ; num_all_user # 19998631
num_all_rrtaps <- sum(all_taps$rrtaps_aC>=covered_C1, na.rm=TRUE) ; num_all_rrtaps # 1272802

# chromatin state 
regionfile <- c("processed/mESC_cStates_HMM.intersectcpg.txt")
regions <- fread(regionfile, header=F, stringsAsFactors = F) %>% as.data.frame()
colnames(regions) <- c("gchr","gstart","gend","id","chr","start","end")
regionsmeth <- merge(regions, all_taps, by=c("chr","start"))
regionsmeth$id <- gsub("[0-9]*_","",regionsmeth$id)
sta <- merge(prop.table(table(regionsmeth[regionsmeth$taps_aC>=covered_C1,6])) %>% as.data.frame(),
             prop.table(table(regionsmeth[regionsmeth$user_total>=covered_C2,6]))%>% as.data.frame(),
             by=c("Var1")) %>%
  merge(prop.table(table(regionsmeth[regionsmeth$rrtaps_aC>=covered_C1,6]))%>% as.data.frame(),
        by=c("Var1"))
colnames(sta) <- c("feature","taps","user","rrtaps")
sta.m <- melt(sta,id.vars=c("feature"))
sta.m$feature <- factor(sta.m$feature, level=c("PoisedEnhancer","PoisedPromoter","Insulator","ActivePromoter","TxnTransition","StrongEnhancer","Repressed","TxnElongation","WeakTxn","Heterochrom"))
p <- ggplot(sta.m,aes(x=feature,y=value*100,fill=variable)) +
  geom_bar(sta="identity",position = "dodge") +
  theme_minimal() +
  ylab("%") +
  scale_fill_manual(values=c("#446db4","#23b177","#fac133"), name=("method number of aC"),
                    labels = c(paste("TAPS",length(which(regionsmeth$taps_aC>=covered_C1))),
                               paste("user-TAPS",length(which(regionsmeth$user_total>=covered_C2))),
                               paste("rrTAPS",length(which(regionsmeth$rrtaps_aC>=covered_C1)))
                    )) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1),
        legend.position = "right")
ggsave(paste0(outfix,"aC_pct_feature_distribution.pdf"), p, width=6,height =3.5 )
sta.m$class <- gsub(".*_","",sta.m$feature)
aggregate(value~variable+class,sta.m,sum)

#           feature     taps     user rrtaps ||           feature        taps        user      rrtaps
# 1  ActivePromoter  1186369  1011941 444447 || 1  ActivePromoter 0.061884813 0.050601255 0.349249319
# 2     Heterochrom 12543254 13660498 361415 || 2     Heterochrom 0.654296370 0.683081664 0.284002238
# 3       Insulator    93108    90620   9866 || 3       Insulator 0.004856812 0.004531377 0.007752766
# 4  PoisedEnhancer    67884    59419  16337 || 4  PoisedEnhancer 0.003541047 0.002971197 0.012837720
# 5  PoisedPromoter   522956   435003 196689 || 5  PoisedPromoter 0.027279063 0.021751958 0.154559485
# 6       Repressed   938874   932270  52584 || 6       Repressed 0.048974680 0.046617374 0.041320846
# 7  StrongEnhancer   846497   819846  63108 || 7  StrongEnhancer 0.044155999 0.040995707 0.049590673
# 8   TxnElongation  1204547  1201923  59458 || 8   TxnElongation 0.062833036 0.060101144 0.046722480
# 9   TxnTransition   176658   174180  15746 || 9   TxnTransition 0.009215048 0.008709724 0.012373308
# 10        WeakTxn  1590453  1612638  52928 || 10        WeakTxn 0.082963131 0.080638601 0.041591164

# fig 2d ---- 
## methylated C sites
ratio <- 0.1 # use fixed ratio
all_taps$taps <- ifelse(all_taps$taps_rmC > ratio & all_taps$taps_aC >= covered_C1, 1, 0); all_taps$taps[is.na(all_taps$taps)] <- 0
all_taps$rrtaps <- ifelse(all_taps$rrtaps_rmC > ratio & all_taps$rrtaps_aC >= covered_C1, 1, 0); all_taps$rrtaps[is.na(all_taps$rrtaps)] <- 0
all_taps$user <- ifelse(all_taps$user_rmC > ratio & all_taps$user_total >= covered_C2, 1, 0); all_taps$user[is.na(all_taps$user)] <- 0
methylated_C_sta1 <- as.data.frame(paste(all_taps$taps, all_taps$rrtaps,all_taps$user,sep="_") %>% table() /1000000)
methylated_C_sta1
grid.newpage()
pdf(paste0(outfix,"mC_0.1.venn.pdf"), width = 4, height = 4)
num_taps <- sum(all_taps$taps=="1",na.rm=TRUE)
num_user <- sum(all_taps$user=="1",na.rm=TRUE)
num_rrtaps <- sum(all_taps$rrtaps=="1",na.rm=TRUE)
num_taps_user <- sum(all_taps$taps=="1"&all_taps$user=="1",na.rm=T)
num_rrtaps_user <- sum(all_taps$rrtaps=="1"&all_taps$user=="1",na.rm=T)
num_taps_rrtaps <- sum(all_taps$taps=="1"&all_taps$rrtaps=="1",na.rm=T)
num_taps_user_rrtaps <- sum(all_taps$taps=="1"&all_taps$rrtaps=="1"&all_taps$user=="1",na.rm=T)
draw.triple.venn(
  area1     = num_taps,
  area2     = num_user,
  area3     = num_rrtaps,
  n12       = num_taps_user,
  n23       = num_rrtaps_user,
  n13       = num_taps_rrtaps,
  n123      = num_taps_user_rrtaps,
  category  = c('taps', 'user', 'rrtaps'),
  fill      = c("#446db4","#23b177","#fac133"),
  cat.col   = c("#446db4","#23b177","#fac133"),
  euler.d = TRUE,
  scaled    = TRUE
)
dev.off()

# fig 2c (eetaps) ----
seltaps <- all_taps[all_taps$taps_aC >= covered_C1 & all_taps$user_total >= covered_C2, ]  %>% dplyr::select(taps_rmC, user_rmC)
seltaps <- seltaps[complete.cases(seltaps),]
round(cor(seltaps$taps_rmC, seltaps$user_rmC),2);nrow(seltaps)
# [1] 0.55
# [1] 18552024
pdf(paste0(outfix,"taps_all.scatter.pdf"), width = 3.2, height = 3.5)
smoothScatter(x=seltaps$taps_rmC,
              y=seltaps$user_rmC,
              xlab="wgTAPS",
              ylab= "eeTAPS",
              xlim=c(0,1),
              ylim=c(0,1))
dev.off()


# fig s2c (rrtaps) ----
seltaps <- all_taps[ all_taps$taps_aC >= covered_C1 & all_taps$rrtaps_aC >= covered_C1, ] %>% dplyr::select(taps_rmC, rrtaps_rmC)
seltaps <- seltaps[complete.cases(seltaps),]
round(cor(seltaps$taps_rmC,seltaps$rrtaps_rmC),2);nrow(seltaps)
# [1] 0.9
# [1] 1237583
pdf(paste0(outfix,"rr_taps_all.scatter.pdf"), width = 3.2, height = 3.5)
smoothScatter(x=seltaps$taps_rmC,
              y=seltaps$rrtaps_rmC,
              xlab="wgTAPS",
              ylab= "rrTAPS",
              xlim=c(0,1),
              ylim=c(0,1))
dev.off()
seltaps$rrtaps_level <- cut(seltaps$rrtaps_rmC, breaks=seq(0, 1, 1/16), include.lowest=TRUE, label=seq(1,16)) %>% as.numeric()
seltaps$taps_level <- cut(seltaps$taps_rmC, breaks=seq(0, 1, 1/16), include.lowest=TRUE, label=seq(1,16)) %>% as.numeric()
sta_level <- table(seltaps %>% dplyr::select(rrtaps_level, taps_level)) %>% as.data.frame()
sta_level$Freq[sta_level$Freq>=578971] <- 30000
sta_level.w <- spread(sta_level, taps_level, Freq)
pheatmap(sta_level.w[,-1], filename=paste0(outfix,"rr_taps_all.heatmap.pdf"), heigh=4, width=5, cluster_cols = FALSE, cluster_rows = FALSE)


# fig s2d ----
## correlation for replicates 
## Enrichment_TAPS_new_bwamem were down-sampled to the same depth as Enrichment_TAPS_bwamem
user_taps1 <- read_delim("processed/Enrichment_TAPS_new_bwamem.sort.mm9.0.66.proper.meth.txt",delim="\t",col_names=TRUE) %>% as.data.frame()
user_taps2 <- read_delim("processed/Enrichment_TAPS_bwamem.mm9.sort.proper.meth.txt",delim="\t",col_names=TRUE) %>% as.data.frame()
all_taps <- merge(user_taps1,user_taps2,by=c("chr","pos"))
colnames(all_taps) <- c("chr","start",
                        "user1_total","user1_cleaved","user1_uncleaved","user1_methratio",
                        "user2_total","user2_cleaved","user2_uncleaved","user2_methratio")
userratio<- 0.1
all_taps$user1 <- apply(all_taps %>% dplyr::select(user1_total, user1_methratio), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > userratio, 1, 0)})
all_taps$user2 <- apply(all_taps %>% dplyr::select(user2_total, user2_methratio), 1, function(x){ifelse(x[1] >= covered_C2 & x[2] > userratio, 1, 0)})
user.vs.rep <- table(all_taps %>% dplyr::select(user1, user2),useNA="ifany") %>% as.data.frame()
grid.newpage()
pdf(paste0(outfix,"user_sub_rep.pdf"),width = 4, height = 4)
ncommon <- user.vs.rep[user.vs.rep$user1=="1" & user.vs.rep$user2=="1" & complete.cases(user.vs.rep),3] %>% as.numeric()
nuser1 <- user.vs.rep[user.vs.rep$user1=="1",3] %>% sum(na.rm=TRUE)
nuser2 <- user.vs.rep[user.vs.rep$user2=="1",3] %>% sum(na.rm=TRUE)
draw.pairwise.venn(nuser1, nuser2, ncommon, category = c("user1", "user2"),
                   lty = rep("blank", 2), fill = c(brewer.pal(4,"Set2")[c(3,1)]),
                   alpha = rep(0.5, 2), cat.pos = c(0,0), cat.dist = rep(0.025, 2))
dev.off()



# fig s2a ----
dat <- read.table("processed/Enrichment_TAPS_new_bwamem.mm9.sort.isize.cxt.sta",header=FALSE)
colnames(dat) <- c("context","number")
dat <-dat[dat$context!="N",]
dat$Freq <- dat$number/sum(dat$number)

p <- ggplot(dat, aes(x="", y=Freq, fill=context))+
  geom_bar(width = 1, stat = "identity", alpha=0.8) +
  coord_polar("y", start=0) +
  scale_fill_manual(name="read end", values=brewer.pal(4,"Set2")[c(2,1,3,4)])+
  theme_bw() +
  theme(axis.text.x=element_blank(),
        text = element_text(size = 14))

ggsave(
  "plots/Enrichment_TAPS_new_bwamem.mm9.sort.isize.cxt.pdf",
  p,
  width = 4,
  height = 4
)

# fig s2b ----
dat <- read.table("processed/Enrichment_TAPS_new_bwamem.mm9.sort.isize.cgdis.sta")
p <- ggplot(dat, aes(x=V1,y=V2)) +
  geom_bar(stat = "identity",fill = "#2880b9") +
  theme_minimal()
ggsave("plots/Enrichment_TAPS_new_bwamem.mm9.sort.isize.cgdis.pdf",p, width=5, height=2.5)
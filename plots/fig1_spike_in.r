library(dplyr)
library(ggplot2)
library(reshape2)
library(gridExtra)

# fig 1b ----
bs <- read.table("processed/BS_4kb.md.meth.CG.depth3.average.methratio.txt",header=TRUE)
user <- read.table("processed/Enrichment_TAPS_new_bwamem.spike_in.sort.proper.meth.txt",header=TRUE)
user_ctl <- read.table("processed/USER_ctrl_bwamem.spike_in.sort.proper.meth.txt",header=TRUE)
colnames(bs)[2] <- c("pos")
cmp <- merge(bs,user,all.x=TRUE,by=c("chr","pos")) %>% merge(user_ctl,all.x=TRUE,by=c("chr","pos"))

cmp.s <- cmp[,c(1,2,3,7,11)]
colnames(cmp.s) <- c("chr","pos","bs","user","user_ctl")
cmp.s <- cmp.s 
cmp.m <- melt(cmp.s,id.vars=c("chr","pos"))

p <- ggplot(cmp.m, aes(x = pos, y = value,color=variable)) +
     geom_bar(stat="identity") +
     facet_wrap(~variable,ncol=1) +
     theme_minimal() +
     ylab("meth") +
     scale_color_manual(values=c("#23b177","#446db4","#4162a4")) +
     theme(legend.position="none",
     panel.grid.major = element_blank(), 
     panel.grid.minor = element_blank(),
     panel.background = element_blank(), 
     axis.line = element_line(colour = "black"))
ggsave(
  paste0("plots/4kb_meth_taps.pdf"),
  p,
  width = 12,
  height = 3 ,
  dpi = 300
)


# fig s1f ----
p <- ggplot(cmp.s, aes(x=bs, y=user)) +
    geom_point(color = "#2880b9", size=0.5) +
    xlim(0,1)+
    ylim(0,1)+
    geom_text(x=max(cmp.s$bs)*0.7, y=max(cmp.s$user)*0.7,label=round(cor(cmp.s$bs,cmp.s$user),2), size=4) +
    theme_minimal()

ggsave(
  "plots/4kb_meth_taps.cor.pdf",
  p,
  width = 5,
  height = 5
)


# fig s1e ----
dat <- read.table("processed/Enrichment_TAPS_new_bwamem.mm9.sort.q0.isize.txt", header=FALSE, sep="\t")
colnames(dat) <- c("isize","nfrag")
p <- ggplot(dat, aes(x=isize,y=nfrag)) +
  geom_bar(stat="identity", fill="#1B9E77")+
  xlim(50,500) + theme_minimal() +
  theme(legend.position = "bottom") 
ggsave("plots/Enrichment_TAPS_new_bwamem.mm9.sort.q0.isize.pdf",p,width=8,height=3.5)


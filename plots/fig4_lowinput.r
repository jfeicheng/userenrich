# load packages & functions ----
rm(list=ls())
library(data.table)
library(dplyr)
library(Smisc)
library(forecast)
library(plyr)
library(viridis)
library(ggplot2)
library(RColorBrewer)
library(readr)
library(grid)
library(VennDiagram)
library(pheatmap)

# load data ----
user_taps_1ng   <- read_delim("processed/Enrichment_TAPS_1ng_bwamem.mm9.sort.proper.meth.txt",delim="\t",col_names=TRUE)
user_taps_10ng  <- read_delim("processed/Enrichment_TAPS_10ng_bwamem.mm9.sort.proper.meth.txt",delim="\t",col_names=TRUE)
user_taps_50ng  <- read_delim("processed/Enrichment_TAPS_50ng_bwamem.mm9.sort.proper.meth.txt",delim="\t",col_names=TRUE)
user_taps_200ng_sub <- read_delim("processed/Enrichment_TAPS_bwamem.merge.mm9.sort.02.proper.meth.txt",delim="\t",col_names=TRUE)

taps <- read_delim("processed/taps_pub.average.methratio.txt",delim="\t",col_names=TRUE)
all_taps <- merge(taps,user_taps_1ng,by.x=c("chr","start"),by.y=c("chr","pos")) %>% 
            merge(user_taps_10ng,by.x=c("chr","start"),by.y=c("chr","pos")) %>%
            merge(user_taps_50ng,by.x=c("chr","start"),by.y=c("chr","pos")) %>%
            merge(user_taps_200ng_sub,by.x=c("chr","start"),by.y=c("chr","pos"))

colnames(all_taps) <- c("chr","start","taps_mC","taps_aC","taps_rmC",
                        "user_total_1ng","user_cleaved_1ng","user_uncleaved_1ng","user_methratio_1ng",
                        "user_total_10ng","user_cleaved_10ng","user_uncleaved_10ng","user_methratio_10ng",
                        "user_total_50ng","user_cleaved_50ng","user_uncleaved_50ng","user_methratio_50ng",
                        "user_total_200ng_sub","user_cleaved_200ng_sub","user_uncleaved_200ng_sub","user_methratio_200ng_sub")

all_taps <- all_taps[order(all_taps$chr,all_taps$start),]
e14_snp <- read_delim("processed/e14.cgsnp.bed",delim="\t", col_names=FALSE)
colnames(e14_snp) <- c("chr","start","end","snp")
all_taps <- merge(all_taps,e14_snp,by=c("chr","start"),all.x=TRUE)


all_taps <- all_taps[!complete.cases(all_taps$end),-c(ncol(all_taps)-1,ncol(all_taps))]

covered_C1 <- 5 # depth_filter for wgTAPS and rrTAPS
covered_C2 <- 1 # depth_filter eeTAPS
usermethratio <- 0.1
mCratio <- 0.1
outfix <- "plots/depth1_depth5"

seltaps <- all_taps[complete.cases(all_taps) & all_taps$taps_aC>=covered_C1,]
seltaps$user_1ng <- apply(seltaps %>% dplyr::select(user_total_1ng, user_methratio_1ng), 1, function(x){ifelse(x[1] >=covered_C2 & x[2] > usermethratio, 1, ifelse(x[1]==0,"NA",0))})
seltaps$user_10ng <- apply(seltaps %>% dplyr::select(user_total_10ng, user_methratio_10ng), 1, function(x){ifelse(x[1] >=covered_C2 & x[2] > usermethratio, 1, ifelse(x[1]==0,"NA",0))})
seltaps$user_50ng <- apply(seltaps %>% dplyr::select(user_total_50ng, user_methratio_50ng), 1, function(x){ifelse(x[1] >=covered_C2 & x[2] > usermethratio, 1, ifelse(x[1]==0,"NA",0))})
seltaps$user_200ng_sub <- apply(seltaps%>% dplyr::select(user_total_200ng_sub, user_methratio_200ng_sub), 1, function(x){ifelse(x[1] >=covered_C2 & x[2] > usermethratio, 1, ifelse(x[1]==0,"NA",0))})
seltaps$taps <- ifelse(seltaps$taps_rmC=="NA","NA",ifelse(seltaps$taps_rmC>mCratio,1,0))

# fig 4a ----
mCsta <- cbind(table(seltaps %>% dplyr::select(user_1ng,taps),useNA="ifany") %>% as.data.frame(),
table(seltaps%>% dplyr::select(user_10ng,taps),useNA="ifany") %>% as.data.frame(),
table(seltaps%>% dplyr::select(user_50ng,taps),useNA="ifany") %>% as.data.frame(),
table(seltaps%>% dplyr::select(user_200ng_sub,taps),useNA="ifany") %>% as.data.frame())

mCsta <- mCsta[,c(2,1,3,6,9,12)]
colnames(mCsta) <- c("taps","user_taps","1ng","10ng","50ng","200ng_sub")
mCsta.m <- melt(mCsta,id.vars=c("taps","user_taps"))
mCsta.m$flag <- paste(mCsta.m$taps, mCsta.m$user_taps,sep="_")
p <- ggplot(mCsta.m[mCsta$taps==1&mCsta.m$user_taps==1,], aes(x=variable,y=value,fill=flag), alpha=0.8) + 
  geom_bar(stat="identity", position = "dodge2") +
  scale_fill_manual(values=c("#446db4"),labels = c("detected by TAPS"),name="") +
  theme(legend.position="none",
      panel.grid.major = element_blank(), 
      panel.grid.minor = element_blank(),
      panel.background = element_blank(), 
      axis.line = element_line(colour = "black"))
ggsave(paste0(outfix,"mC.sta.sub.pdf"), p, width=3.8,height =3.5 )
# covered_C1 <- 5; covered_C2 <- 1
#   taps user_taps     1ng     10ng     50ng 200ng_sub
# 1    0         0  561593   554496   686825    797763
# 2    0         1   55648    63968    81563    109335
# 3    0        NA 1208138  1206915  1056991    918281
# 4    1         0 2963443  2785149  2785387   2358369
# 5    1         1 7653774 10181162 11563420  13003348
# 6    1        NA 6728255  4379161  2996665   1983755
# mCsta[mCsta$taps==1&mCsta$user_taps==1,-c(1:2)]/(2963443+7653774+6728255)
#                 0.4412549 0.5869637 0.6666535 0.7496682

# fig 4c ----
winsize <- 100000
seldat <- seltaps[seltaps$user_total_1ng >=covered_C2 & seltaps$user_total_10ng >= covered_C2 & seltaps$user_total_50ng >= covered_C2 & seltaps$user_total_200ng_sub >= covered_C2 ,]
print(dim(seldat));print(dim(seltaps))
seldat$index <- round(seldat$start/winsize)
user_1ng_mc <- aggregate(user_methratio_1ng ~ chr+index, seldat, mean)
user_10ng_mc <- aggregate(user_methratio_10ng ~ chr+index, seldat, mean)
user_50ng_mc <- aggregate(user_methratio_50ng ~ chr+index, seldat, mean)
user_200ng_sub_mc <- aggregate(user_methratio_200ng_sub ~ chr+index, seldat, mean)

selmC <- merge(user_1ng_mc, user_10ng_mc, by=c("chr","index")) %>% 
    merge(user_50ng_mc, by=c("chr","index")) %>%
    merge(user_200ng_sub_mc, by=c("chr","index"))

colnames(selmC) <- c("chr","index","user_1ng_mc","user_10ng_mc","user_50ng_mc","user_200ng_sub_mc")

cor(selmC[,-c(1:2)])

# covered_C1 <- 5; covered_C2 <- 1
#                   user_1ng_mc user_10ng_mc user_50ng_mc user_200ng_sub_mc
# user_1ng_mc         1.0000000    0.8887704    0.9057241         0.9080217
# user_10ng_mc        0.8887704    1.0000000    0.9265226         0.9235107
# user_50ng_mc        0.9057241    0.9265226    1.0000000         0.9338444
# user_200ng_sub_mc   0.9080217    0.9235107    0.9338444         1.0000000

pdf(paste0(outfix,"1ng.vs.200ng.100k.cor.sub.pdf"), width = 3.5, height = 3.5)
smoothScatter(x=selmC$user_200ng_sub_mc,
              y=selmC$user_1ng_mc,
              xlab="200ng",
              ylab= "1ng",
              xlim=c(0,1),
              ylim=c(0,1))
dev.off()

pdf(paste0(outfix,"10ng.vs.200ng.100k.cor.sub.pdf"), width = 3.5, height = 3.5)
smoothScatter(x=selmC$user_200ng_sub_mc,
              y=selmC$user_10ng_mc,
              xlab="200ng",
              ylab= "10ng",
              xlim=c(0,1),
              ylim=c(0,1))
dev.off()

pdf(paste0(outfix,"50ng.vs.200ng.100k.cor.sub.pdf"), width = 3.5, height = 3.5)
smoothScatter(x=selmC$user_200ng_sub_mc,
              y=selmC$user_50ng_mc,
              xlab="200ng",
              ylab= "50ng",
              xlim=c(0,1),
              ylim=c(0,1))
dev.off()

# fig 4b ----
pheatmap(selmC[,-c(1:2)], cluster_cols = FALSE,
  filename = paste0(outfix,"100k.mC.heatmap.ori.sub.pdf"),
  show_rownames = FALSE,
  breaks =seq(0,1,1/100))

print(nrow(seldat));print(nrow(seltaps));print(nrow(seldat)/nrow(seltaps))
# covered_C1 <- 5; covered_C2 <- 1
# [1] 7611312
# [1] 19170851
# [1] 0.3970253


# Introduction
This method is based on the conversion of 5mC to dihydrouracil (DHU) through TET assisted 2-Picoline borane treatment, followed by USER (Uracil-Specific Excision Reagent) cleavage of the DHU site.   
> Dir: /users/ludwig/cfo155/cfo155/userEnrich


# Preprocess: from fastq to eeTAPS stats

## 01.mapping.sh
    * reference genome:
    mm9
    * spike-in:   
    1st run: un-methylated 2kb + CG methylated lambda   
    2nd run: CCGG methylated 4kb + unmethylated lambda   
    3rd run: CCGG methylated 4kb + unmethylated lambda

## 02.genomeCG.sh
    * find the CG sites both in mm9 and spike-in

## 03.preprocess/03.filter_read_cg.sh
    * get methylation stats info for eeTAPS: 
    
    chr | pos |total | cleaved | uncleaved | methratio|


# Plots


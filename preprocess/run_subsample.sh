#!/bin/bash

#$ -N subsample
#$ -P ludwig.prjc
#$ -q short.qc
#$ -cwd
#$ -o run_subsample.log
#$ -e run_subsample.err
#$ -pe shmem 8

# Some useful data about the job to help with debugging
echo "------------------------------------------------"
echo "SGE Job ID: $JOB_ID"
echo "SGE Task ID: $SGE_TASK_ID"
echo "Run on host: "`hostname`
echo "Operating system: "`uname -s`
echo "Username: "`whoami`
echo "Started at: "`date`
echo "Argument: $@"
echo "------------------------------------------------"

# set PATH
PATH=$PATH:/users/ludwig/cfo155/miniconda2/bin
export PATH

# set dir
WORKDIR=/users/ludwig/cfo155/cfo155/userEnrich
cd $WORKDIR

##### Subsample mm9 bam files #####
n1=`samtools view -c processed/Enrichment_TAPS_bwamem.mm9.sort.bam`
n2=`samtools view -c processed/Enrichment_TAPS_new_bwamem.mm9.sort.bam`
for n in 16000000 32000000 64000000 96000000 128000000
do
  p1=`echo "scale=2;$n/$n1"|bc`
  p2=`echo "scale=2;$n/$n2"|bc`
  d=`echo "scale=2;$n/32000000"|bc`
  samtools view -bS -s $p1 processed/Enrichment_TAPS_bwamem.mm9.sort.bam \
   >processed/Enrichment_TAPS_bwamem.sort.mm9.$d.bam
  samtools view -bS -s $p2 processed/Enrichment_TAPS_new_bwamem.mm9.sort.bam  \
   >processed/Enrichment_TAPS_new_bwamem.sort.mm9.$d.bam
done

samtools merge processed/Enrichment_TAPS_bwamem.merge.mm9.bam  \
 processed/Enrichment_TAPS_bwamem.mm9.sort.bam   \
 processed/Enrichment_TAPS_new_bwamem.mm9.sort.bam 

samtools sort processed/Enrichment_TAPS_bwamem.merge.mm9.bam  \
 -o processed/Enrichment_TAPS_bwamem.merge.mm9.sort.bam 

for s in `seq -s ' ' 0.1 0.1 1 `
do
  samtools view -bS -s $s processed/Enrichment_TAPS_bwamem.merge.mm9.sort.bam \
   >processed/Enrichment_TAPS_bwamem.merge.mm9.$s.bam
done

##### Subsample spike-in bam files #####
for s in `seq -s ' ' 0.1 0.1 1 `
do
  samtools view -bS -s $s processed/Enrichment_TAPS_new_bwamem.spike_in.sort.bam \
   >processed/Enrichment_TAPS_new_bwamem.spike_in.$s.bam
  samtools view -bS -s $s processed/Enrichment_TAPS_bwamem.spike_in.sort.bam \
   >processed/Enrichment_TAPS_bwamem.spike_in.$s.bam
done

#!/bin/bash

#$ -N baminfo
#$ -P ludwig.prjc
#$ -q short.qc
#$ -cwd
#$ -o 05.filter_read.log
#$ -e 05.filter_read.err
#$ -pe shmem 4

# Some useful data about the job to help with debugging
echo "------------------------------------------------"
echo "SGE Job ID: $JOB_ID"
echo "SGE Task ID: $SGE_TASK_ID"
echo "Run on host: "`hostname`
echo "Operating system: "`uname -s`
echo "Username: "`whoami`
echo "Started at: "`date`
echo "Argument: $@"
echo "------------------------------------------------"

# set PATH
PATH=$PATH:/users/ludwig/cfo155/miniconda2/bin
export PATH

# set dir
WORKDIR=/users/ludwig/cfo155/cfo155/userEnrich/processed/test
cd $WORKDIR

bam=$1
CGsite=$2
genome=$3
# CGsite=4kb_lambda.cg.bed; genome=spike_in.fa
# CGsite=mm9_genome.cg.sort.bed; genome=mm9_genome.fa
picard=/users/ludwig/cfo155/miniconda2/pkgs/picard-2.18.29-0/share/picard-2.18.29-0/picard.jar 
cleaveddis=${bam/.bam/}.cleaved.cgdis.txt # chr, start, end, star_cgdis, end_cgdis, qname
cleaved_depth_1cg=${bam/.bam/}.cleaved_depth.cgdis0.1cg.txt # chr, pos, cleaved_depth (# qname with at least 1 cg)
proper_bam=${bam/.bam/}.proper.bam # bam with propermapped flag
proper_depth=${bam/.bam/}.proper.total_depth.txt # bam with both cg
proper_vcf=${bam/.bam/}.proper.vcf # bam with both cg
proper_mC=${bam/.bam/}.proper.uncleaved.mC.txt # bam with both cg
proper_meth=${bam/.bam/}.proper.meth.txt # chr, pos, total, cleaved, uncleaved, methratio


##### distance between cleaved sites to CpG #####
samtools view $bam |\
awk 'BEGIN{OFS="\t"}{if($2==99||$2==163 )print $3,$4-1,$4-1,$3,$4+$9-1-1,$4+$9-1-1,$1}' |\
sort -k1,1 -k2,2n |\
bedtools closest -nonamecheck \
-a - -b $CGsite -D b |\
awk 'BEGIN{OFS="\t"}{print$4,$5,$6,$1,$2,$3,$12,$7}' |\
sort -k1,1 -k2,2n |\
bedtools closest -nonamecheck \
-a - -b $CGsite -D b |\
awk 'BEGIN{OFS="\t"}{print $4,$5,$2,$7,$13,$8 }' >$cleaveddis

##### cleaved depth #####
# fragment one end with cg
awk 'BEGIN{OFS="\t"}{print $1,$2,$4,$6"\n"$1,$3,$5,$6}' $cleaveddis|\
awk 'BEGIN{OFS="\t"}{if($3==0)print $1,$2,$4}' |sort -u |cut -f1-2| sort |uniq -c |sed 's/^ \+//g;s/ /\t/g'|\
awk 'BEGIN{OFS="\t"}{print $2,$3,$1}' |sort -k1,1 -k2,2n >$cleaved_depth_1cg

#### total depth #####
bedtools intersect -nonamecheck \
 -a <(awk 'BEGIN{OFS="\t"}{print $1,$2,$3+1,$4}' $CGsite |sort -k1,1 -k2,2n ) \
 -b <(samtools view $bam |awk 'BEGIN{OFS="\t"}{if($2==99||$2==163 )print $3,$4-1"\t"$4+$9-1,$1}' |sort -k1,1 -k2,2n ) -wa -wb -sorted|\
 cut -f1,2,3,8|sort -u|cut -f1-3 |sort |uniq -c |sed 's/^ \+//g;s/ /\t/g'|\
 awk 'BEGIN{OFS="\t"}{print $2,$3,$1}' |sort -k1,1 -k2,2n >$proper_depth

##### get uncleaved sites #####
# for proper mapped bam
samtools view -h $bam |awk '$0~/^@/||$2==99||$2==147||$2==83||$2==163' |\
    samtools view -bS - >$proper_bam
bcftools mpileup -f $genome $proper_bam -a AD,ADF,ADR,DP --max-depth 200000000 > $proper_vcf
awk 'BEGIN{OFS="\t"}{if($0!~/^#/ && $8~/^DP/)print $1,$2,$4,$4","$5,$10}' $proper_vcf |\
    sed 's/:/\t/g'|\
    awk 'BEGIN{OFS="\t"}{if($3=="C" && $4~/T/){split($9,counts,",");idx1=int((index($4,"T")+1)/2);print $1,$2,$3,$4,$9,counts[idx1]}
                    else if($3=="G" && $4~/A/){split($9,counts,",");idx1=int((index($4,"A")+1)/2);print $1,$2,$3,$4,$9,counts[idx1]}}' |\
    sed 1i"chr\tpos\tref\talt\tAD\tmC" >$proper_mC

##### get uncleaved sites #####
# for proper mapped
join -1 1 <(awk 'BEGIN{OFS="\t"}{print $1"_"$2,$1"_"$2"\n"$1"_"$3,$1"_"$2}' $CGsite|sort -k1,1 ) \
     -2 1 <(awk 'BEGIN{OFS="\t"}{print $1"_"$2,$3}' $proper_depth|sort -k1,1 ) -a1 -t$'\t' |  \
    awk 'BEGIN{OFS="\t"}{if($3=="")print $1,$2,"0";else print $1,$2,$3}' |sort -k1,1  |\
    join -1 1 - -2 1 <(awk 'BEGIN{OFS="\t"}{print $1"_"$2,$3}' $cleaved_depth_1cg|sort -k1,1 ) -a1 -t$'\t' |\
    awk 'BEGIN{OFS="\t"}{if($4=="")print $1,$2,$3,"0";else print $1,$2,$3,$4}' |sort -k1,1 |\
    join -1 1 - -2 1 <(awk 'BEGIN{OFS="\t"}{print $1"_"$2-1,$6}' $proper_mC|sort -k1,1 ) -a1 -t$'\t' |\
    awk 'BEGIN{OFS="\t"}{if($5=="")print $1,$2,$3,$4,"0";else print $1,$2,$3,$4,$5}' |cut -f2- |\
    awk -v OFS='\t' '{depth[$1]+=$2;cleaved[$1]+=$3;uncleaved[$1]+=$4}END{for(i in depth){
    if(depth[i]!=0)print i,depth[i],cleaved[i],uncleaved[i],(cleaved[i]+uncleaved[i])/depth[i];else print i,depth[i],cleaved[i],uncleaved[i],0}}' |\
    sed 's/_/\t/g'|sort -k1,1 -k2,2n |\
    awk 'BEGIN{OFS="\t"}{if($6>1)print $1,$2,$3,$4,$5,"0";else print $0}'|\
    sed 1i"chr\tpos\ttotal\tcleaved\tuncleaved\tmethratio" > $proper_meth

# for i in `ls *mm9*bam`
# do
#     qsub 05.filter_read_cg.sh $i mm9_genome.cg.sort.bed mm9_genome.fa
# done
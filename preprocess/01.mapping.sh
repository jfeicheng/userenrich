#!/bin/bash

#$ -N enrich
#$ -P ludwig.prjc
#$ -q short.qc
#$ -cwd
#$ -o run_mapping.rep3.log
#$ -e run_mapping.rep3.err
#$ -pe shmem 8

# Some useful data about the job to help with debugging
echo "------------------------------------------------"
echo "SGE Job ID: $JOB_ID"
echo "SGE Task ID: $SGE_TASK_ID"
echo "Run on host: "`hostname`
echo "Operating system: "`uname -s`
echo "Username: "`whoami`
echo "Started at: "`date`
echo "Argument: $@"
echo "------------------------------------------------"

# set PATH
PATH=$PATH:/users/ludwig/cfo155/miniconda2/bin
export PATH

# set dir
WORKDIR=/users/ludwig/cfo155/cfo155/userEnrich
cd $WORKDIR

##### SNP in E14 #####
wget -c http://epigenetics.hugef-research.org/downloads/SNV.vcf
bedtools getfasta -bed  <(grep -v "^#" SNV.vcf |awk 'BEGIN{OFS="\t"}{if($4=="C")print $1,$2-1,$2+1,$4;else if($4=="G") print $1,$2-2,$2,$4}' ) \
                  -fi mm9_genome.fa -tab |awk 'BEGIN{OFS="\t"}{if(toupper($2)=="CG")print $1,toupper($2)}' |sed 's/:/\t/g;s/-/\t/g' > e14.cgsnp.bed 

########################################################################################################################                  
##### Rep3 #####
for id in Enrichment_TAPS_10ng Enrichment_TAPS_1ng Enrichment_TAPS_50ng Enrichment_TAPS_Endo_III USER_ctrl
do
  cat rawdata/$id/*/*R1_001.fastq.gz >rawdata/${id}_R1.fastq.gz
  cat rawdata/$id/*/*R2_001.fastq.gz >rawdata/${id}_R2.fastq.gz
  trim_galore -o rawdata/ \
    --paired --retain_unpaired --length 35 --gzip \
    rawdata/${id}_R1.fastq.gz rawdata/${id}_R2.fastq.gz
  genome=resource/mm9_genome.4kb.lambda.fa
  bwa mem $genome rawdata/${id}_R1_val_1.fq.gz rawdata/${id}_R2_val_2.fq.gz -t 10 |\
    samtools view -bS - |samtools sort - -o processed/${id}_bwamem.sort.bam -m 2G -@ 10 
  # split alignment into genome & spike-in 
  samtools view -b -L resource/mm9_genome.bed processed/${id}_bwamem.sort.bam \
   >processed/${id}_bwamem.mm9.sort.bam 
  samtools view -b -L resource/4kb_lambda.bed  processed/${id}_bwamem.sort.bam \
   >processed/${id}_bwamem.spike_in.sort.bam 
done

########################################################################################################################
##### trim_galore  and alignment were done sperately since the reference is different #####
##### trim_galore #####
for id in Enrichment_TAPS Enrichment_TAPS_new
do
  trim_galore -o rawdata/ \
    --paired --retain_unpaired --length 35 --gzip \
    rawdata/${id}_R1.fastq.gz rawdata/${id}_R2.fastq.gz
done


##### Rep2 #####
# generate genome
cat resource/mm9_genome.fa \
 /users/ludwig/cfo155/cfo155/ontTaps/resource/4kb.fa \
 /users/ludwig/cfo155/cfo155/17042019/resource/bsgenome/lambda.fasta \
 > resource/mm9_genome.4kb.lambda.fa 
samtools faidx resource/mm9_genome.4kb.lambda.fa 
awk 'BEGIN{OFS="\t"}{print $1,"0",$2}' resource/mm9_genome.4kb.lambda.fa.fai |\
grep chr  >resource/mm9_genome.bed 
awk 'BEGIN{OFS="\t"}{print $1,"0",$2}' resource/mm9_genome.4kb.lambda.fa.fai |\
grep -v chr >resource/4kb_lambda.bed 

# alignment 
genome=resource/mm9_genome.4kb.lambda.fa
id=Enrichment_TAPS_new
bwa index $genome
bwa mem $genome rawdata/${id}_R1_val_1.fq.gz rawdata/${id}_R2_val_2.fq.gz -t 10 |\
  samtools view -bS - |samtools sort - -o processed/${id}_bwamem.sort.bam -m 2G -@ 10 
# split alignment into genome & spike-in 
samtools view -b -L resource/mm9_genome.bed processed/${id}_bwamem.sort.bam \
 >processed/${id}_bwamem.mm9.sort.bam 
samtools view -b -L resource/4kb_lambda.bed  processed/${id}_bwamem.sort.bam \
 >processed/${id}_bwamem.spike_in.sort.bam 

##### Rep1 #####
# generate genome
cat resource/mm9_genome.fa \
 /users/ludwig/cfo155/cfo155/17042019/resource/bsgenome/unmodified_2kb.fa \
 /users/ludwig/cfo155/cfo155/17042019/resource/bsgenome/lambda.fasta \
 > resource/mm9_genome.2kb.lambda.fa 
samtools faidx resource/mm9_genome.2kb.lambda.fa 
awk 'BEGIN{OFS="\t"}{print $1,"0",$2}' resource/mm9_genome.2kb.lambda.fa.fai |\
grep -v chr >resource/2kb_lambda.bed 

# alignment 
genome=resource/mm9_genome.2kb.lambda.fa
id=Enrichment_TAPS
bwa index $genome
bwa mem $genome rawdata/${id}_R1_val_1.fq.gz rawdata/${id}_R2_val_2.fq.gz -t 10 |\
  samtools view -bS - |samtools sort - -o processed/${id}_bwamem.sort.bam -m 2G -@ 10
# split alignment into genome & spike-in 
samtools view -b -L resource/mm9_genome.bed processed/${id}_bwamem.sort.bam \
 >processed/${id}_bwamem.mm9.sort.bam 
samtools view -b -L resource/2kb_lambda.bed  processed/${id}_bwamem.sort.bam \
 >processed/${id}_bwamem.spike_in.sort.bam 
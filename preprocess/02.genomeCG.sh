#!/bin/bash

#$ -N genomeCG
#$ -P ludwig.prjc
#$ -q short.qc
#$ -cwd
#$ -o run_genomeCG.log
#$ -e run_genomeCG.err
#$ -pe shmem 8




# Some useful data about the job to help with debugging
echo "------------------------------------------------"
echo "SGE Job ID: $JOB_ID"
echo "SGE Task ID: $SGE_TASK_ID"
echo "Run on host: "`hostname`
echo "Operating system: "`uname -s`
echo "Username: "`whoami`
echo "Started at: "`date`
echo "Argument: $@"
echo "------------------------------------------------"

# set PATH
PATH=$PATH:/users/ludwig/cfo155/miniconda2/bin
export PATH

# set dir
WORKDIR=/users/ludwig/cfo155/cfo155/userEnrich
cd $WORKDIR

##### Find CG site in genome #####
getCGPos(){
  sequences=$1
  mkdir temp
  fasta_formatter -i $sequences -t|sed 's/>//g'|\
  awk 'BEGIN{OFS="\t"}{print $2>"temp/"$1".fa"}' # one sequence for each chr
  if test -f ${sequences/.fa/}.cg.bed; then rm ${sequences/.fa/}.cg.bed -rf ; fi
  for sequence in `ls temp/*.fa` # get CG position
  do
    chr=`echo $sequence |cut -d '/' -f2|sed 's/.fa//g'`
    grep -aob -i CG $sequence |sed 's/:/\t/g'|\
    awk -v chr=${chr} 'BEGIN{OFS="\t"}{print chr,$1,$1+1,$2}' >>${sequences/.fa*/}.cg.bed
  done
  rm temp -rf 
}
getCGPos resource/mm9_genome.4kb.lambda.fa 
getCGPos resource/mm9_genome.2kb.lambda.fa

grep ^chr resource/mm9_genome.4kb.lambda.cg.bed >resource/mm9_genome.cg.bed 
grep -v ^chr resource/mm9_genome.4kb.lambda.cg.bed >resource/4kb_lambda.cg.bed
grep -v ^chr resource/mm9_genome.2kb.lambda.cg.bed >resource/2kb_lambda.cg.bed

##### Find AT site in genome #####
getATPos(){
  sequences=$1
  mkdir temp
  fasta_formatter -i $sequences -t|sed 's/>//g'|\
  awk 'BEGIN{OFS="\t"}{print $2>"temp/"$1".fa"}' # one sequence for each chr
  if test -f ${sequences/.fa/}.AT.bed; then rm ${sequences/.fa/}.AT.bed -rf ; fi
  for sequence in `ls temp/*.fa` # get AT position
  do
    chr=`echo $sequence |cut -d '/' -f2|sed 's/.fa//g'`
    grep -aob -i AT $sequence |sed 's/:/\t/g'|\
    awk -v chr=${chr} 'BEGIN{OFS="\t"}{print chr,$1,$1+1,$2}' >>${sequences/.fa*/}.AT.bed
  done
  rm temp -rf 
}

getATPos resource/mm9_genome.4kb.lambda.fa
grep -v chr resource/mm9_genome.4kb.lambda.AT.bed > resource/4kb_lambda.AT.bed
